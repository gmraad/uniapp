/* create registration system database */
CREATE DATABASE regsystem;

/* switch to regsystem */
USE regsystem;

/* create user table, where each entry has an id (also primary key), a name, and an e-mail */  
CREATE TABLE user ( 
	id int unsigned not null, 
	name varchar(255) not null, 
	email varchar(255) not null,
	usertype varchar(255) not null,
	PRIMARY KEY (id) 
	);
	
/* add constraint that e-mail be unique */
ALTER TABLE user
ADD CONSTRAINT EMAIL UNIQUE (email);
	
/* populate user table with a few entries */
INSERT INTO user VALUES (
	123456,
	"Sarah",
	"sarah@uchicago.edu",
	"STUDENT"
);

INSERT INTO user VALUES (
	123457,
	"Emily",
	"em@uchicago.edu",
	"STUDENT"
);

INSERT INTO user VALUES (
	123458,
	"Mike",
	"mike@uchicago.edu",
	"STUDENT"
);

INSERT INTO user VALUES (
	123459,
	"Joe",
	"joe@uchicago.edu",
	"STUDENT"
);

INSERT INTO user VALUES (
	15000,
	"Martin",
	"profmartin@uchicago.edu",
	"FACULTY"
);

INSERT INTO user VALUES (
	15001,
	"Brad",
	"brad@uchicago.edu",
	"FACULTY"
);

INSERT INTO user VALUES (
	15002,
	"Jill",
	"drjill@uchicago.edu",
	"FACULTY"
);

INSERT INTO user VALUES (
	100,
	"Alan",
	"adminalan@uchicago.edu",
	"ADMINISTRATOR"
);

/* ************************** */
/* create student table */

CREATE TABLE student (
	email varchar(255) not null,
	hasRestrictions boolean not null,
	PRIMARY KEY(email)
);

ALTER TABLE student
ADD CONSTRAINT unique_email_student_table UNIQUE (email);

INSERT INTO student VALUES (
	"sarah@uchicago.edu", 
	FALSE
);

INSERT INTO student VALUES (
	"em@uchicago.edu", 
	FALSE
);

INSERT INTO student VALUES (
	"mike@uchicago.edu", 
	TRUE
);

INSERT INTO student VALUES (
	"joe@uchicago.edu", 
	FALSE
);


/* ************************** */


/* create courses table, where each entry has a faculty id (also primary key) and the corresponding user id */

CREATE TABLE course (
	id int unsigned not null auto_increment,
	department VARCHAR(255) not null,
	courseNum int not null,
	quarter VARCHAR(255) not null,
	facultyEmail VARCHAR(255),
	roomName VARCHAR(255),
	isOpen boolean not null,
	alias varChar(765) not null,
	PRIMARY KEY (id)
);

ALTER TABLE course 
ADD CONSTRAINT unique_course_alias UNIQUE (alias);

INSERT INTO course VALUES (
	1,
	"MPCS",
	54300,
	"spring18",
	"profmartin@uchicago.edu",
	"young306",
	TRUE,
	"MPCS-54300-spring18"
);

INSERT INTO course VALUES (
	2,
	"MATH",
	10100,
	"spring18",
	"brad@uchicago.edu",
	"young306",
	TRUE,
	"MATH-10100-spring18"
);

INSERT INTO course VALUES (
	3,
	"HIST",
	20200,
	"spring18",
	"drjill@uchicago.edu",
	"young306",
	TRUE,
	"HIST-20200-spring18"
);

INSERT INTO course VALUES (
	4,
	"MPCS",
	52411,
	"spring18",
	"profmartin@uchicago.edu",
	"young306",
	TRUE,
	"MPCS-52411-spring18"
);

INSERT INTO course (department, courseNum, quarter, facultyEmail, roomName, isOpen, alias) VALUES (
	"MPCS",
	52412,
	"spring18",
	"profmartin@uchicago.edu",
	"young306",
	TRUE,
	"MPCS-52412-spring18"
);

/* **************** */

/* create an enrollment table, where each entry has an id (also primary key), a studentid, a courseid, and a grade */ 

CREATE TABLE enrollment (
	id int unsigned not null auto_increment,
	studentEmail VARCHAR(255) not null,
	courseAlias VARCHAR(255) not null,
	grade VARCHAR(3) not null,
	alias VARCHAR(510) not null,
	PRIMARY KEY (id)
);

ALTER TABLE enrollment
ADD CONSTRAINT unique_enrollment_alias UNIQUE (alias);

INSERT INTO enrollment VALUES (
	1,
	"joe@uchicago.edu",
	"MPCS-54300-spring18",
	"TBD",
	"joe-MPCS-54300-spring18"
);

INSERT INTO enrollment VALUES (
	2,
	"sarah@uchicago.edu",
	"MPCS-54300-spring18",
	"TBD",
	"sarah-MPCS-54300-spring18"
);

INSERT INTO enrollment VALUES (
	3,
	"em@uchicago.edu",
	"MPCS-54300-spring18",
	"TBD",
	"em-MPCS-54300-spring18"
);

INSERT INTO enrollment VALUES (
	4,
	"joe@uchicago.edu",
	"MATH-10100-spring18",
	"TBD",
	"joe-MATH-10100-spring18"
);

INSERT INTO enrollment VALUES (
	5,
	"em@uchicago.edu",
	"HIST-20200-spring18",
	"TBD",
	"em-HIST-20200-spring18"
);

INSERT INTO enrollment VALUES (
	6,
	"sarah@uchicago.edu",
	"MATH-10100-spring18",
	"TBD",
	"sarah-MATH-10100-spring18"
);

/* ********************* */

/* create a quarter table, where each entry has an id, a start date, an end date, a reg start date, and a reg end date */

CREATE TABLE quarter (
	id int unsigned not null auto_increment,
	alias varchar(20) not null,
	startdate date not null,
	enddate date not null,
	startregdate date not null,
	endregdate date not null,
	PRIMARY KEY (id)
);

ALTER TABLE quarter
ADD CONSTRAINT unique_quarter_alias UNIQUE (alias);

INSERT INTO quarter VALUES (
	1,
	"spring18",
	DATE '2018-04-01',
	DATE '2018-05-31',
	DATE '2018-03-01',
	DATE '2018-03-31'
);

/* ******************* */

/* create room table, where each entry has a name and a capacity */

CREATE TABLE room (
    id int not null AUTO_INCREMENT,
    name varchar(255) not null,
    capacity int,
    PRIMARY KEY (id)
);

ALTER TABLE room 
ADD CONSTRAINT unique_name UNIQUE (name);

INSERT INTO room VALUES (
	1,
	"young301",
	250
);

INSERT INTO room VALUES (
	2,
	"ryerson255",
	250
);

