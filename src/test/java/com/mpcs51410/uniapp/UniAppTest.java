package com.mpcs51410.uniapp;
import com.mpcs51410.uniapp.model.*;
import org.junit.*;
import static org.junit.Assert.*;
import java.sql.Time;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class UniAppTest {
    @Before
    public void setupTest() {
    }

    // **************** Administrator Tests *****************
    // ******************************************************

    // testing whether an administrator can
    // successfully add a course to the system
    @Test
    public void testAdminAddCourseToSystem() {
        System.out.println("Running testAdminAddCourseToSystem");
        // create administrator
        Administrator alanAdmin = new Administrator("Alan", "alan@uchicago.edu", 100);
        // create quarters
        Date spring18Start = new GregorianCalendar(2018, Calendar.APRIL, 1).getTime();
        Date spring18End = new GregorianCalendar(2018, Calendar.MAY, 31).getTime();
        Date spring18RegStart = new GregorianCalendar(2018, Calendar.FEBRUARY, 15).getTime();
        Date spring18RegEnd = new GregorianCalendar(2018, Calendar.MARCH, 31).getTime();
        Quarter spring18 = new Quarter("spring18", spring18Start, spring18End, spring18RegStart, spring18RegEnd);
        // admin adds course to system
        Course addedCourse = alanAdmin.addCourseToSystem(Department.MATH, 50000, spring18);
        Course courseInSystem = DatabaseConnectivity.getCourseByAlias(addedCourse.getAlias());
        Course math50000ToAdd = new Course(Department.MATH, 50000, spring18);
        // check if successful
        assertTrue(courseInSystem.equals(math50000ToAdd));
        // force delete the added course after assertion is true
        DatabaseConnectivity.forceQuery("delete from course where alias='" + courseInSystem.getAlias() +"'");
    }

    // testing whether an administrator can
    // successfully delete a course from the system
    @Test
    public void testAdminDeleteCourseFromSystem() {
        // NOTE: make sure hist20200 is preloaded in DB before running test
        System.out.println("Running testAdminDeleteCourseFromSystem");
        // create administrator
        Administrator alanAdmin = new Administrator("Alan", "alan@uchicago.edu", 100);
        // create quarters
        Date spring18Start = new GregorianCalendar(2018, Calendar.APRIL, 1).getTime();
        Date spring18End = new GregorianCalendar(2018, Calendar.MAY, 31).getTime();
        Date spring18RegStart = new GregorianCalendar(2018, Calendar.FEBRUARY, 15).getTime();
        Date spring18RegEnd = new GregorianCalendar(2018, Calendar.MARCH, 31).getTime();
        Quarter spring18 = new Quarter("spring18", spring18Start, spring18End, spring18RegStart, spring18RegEnd);
        // first add course to system
        Course hist20200Preloaded = alanAdmin.addCourseToSystem(Department.HIST, 20200, spring18);
        // then delete said course
        Course course = alanAdmin.deleteCourseFromSystem(hist20200Preloaded);
        assertEquals(course, hist20200Preloaded);
    }

    // testing whether an administration
    // can open a course a course for registration
    @Test
    public void testAdminSetCourseOn() {
        System.out.println("Running testAdminSetCourseOn");
        // create administrator
        Administrator alanAdmin = new Administrator("Alan", "alan@uchicago.edu", 100);
        // create quarters
        Date spring18Start = new GregorianCalendar(2018, Calendar.APRIL, 1).getTime();
        Date spring18End = new GregorianCalendar(2018, Calendar.MAY, 31).getTime();
        Date spring18RegStart = new GregorianCalendar(2018, Calendar.FEBRUARY, 15).getTime();
        Date spring18RegEnd = new GregorianCalendar(2018, Calendar.MARCH, 31).getTime();
        Quarter spring18 = new Quarter("spring18", spring18Start, spring18End, spring18RegStart, spring18RegEnd);
        // first add course to system
        Course mpcs51410 = alanAdmin.addCourseToSystem(Department.MPCS, 51410, spring18);
        // update whether the course is open
        alanAdmin.setIfCourseIsOpen(mpcs51410, false);
        alanAdmin.setIfCourseIsOpen(mpcs51410, true);
        assertTrue(mpcs51410.getIsOpen());
        // delete course when done
        alanAdmin.deleteCourseFromSystem(mpcs51410);
    }

    // testing whether an administrator
    // can close a course for registration
    @Test
    public void testAdminSetCourseOff() {
        System.out.println("Running testAdminSetCourseOff");
        System.out.println("Running testAdminSetCourseOn");
        // create administrator
        Administrator alanAdmin = new Administrator("Alan", "alan@uchicago.edu", 100);
        // create quarters
        Date spring18Start = new GregorianCalendar(2018, Calendar.APRIL, 1).getTime();
        Date spring18End = new GregorianCalendar(2018, Calendar.MAY, 31).getTime();
        Date spring18RegStart = new GregorianCalendar(2018, Calendar.FEBRUARY, 15).getTime();
        Date spring18RegEnd = new GregorianCalendar(2018, Calendar.MARCH, 31).getTime();
        Quarter spring18 = new Quarter("spring18", spring18Start, spring18End, spring18RegStart, spring18RegEnd);
        // first add course to system
        Course mpcs51410 = alanAdmin.addCourseToSystem(Department.MPCS, 51410, spring18);
        // update whether the course is open
        alanAdmin.setIfCourseIsOpen(mpcs51410, true);
        alanAdmin.setIfCourseIsOpen(mpcs51410, false);
        assertFalse(mpcs51410.getIsOpen());
        // delete course when done
        alanAdmin.deleteCourseFromSystem(mpcs51410);
    }

    // testing whether administrator
    // can schedule a course
    @Test
    public void testAdminScheduleCourse() {
        System.out.println("Running testAdminScheduleCourse");
        // create administrator
        Administrator alanAdmin = new Administrator("Alan", "alan@uchicago.edu", 100);
        // create quarters
        Date spring18Start = new GregorianCalendar(2018, Calendar.APRIL, 1).getTime();
        Date spring18End = new GregorianCalendar(2018, Calendar.MAY, 31).getTime();
        Date spring18RegStart = new GregorianCalendar(2018, Calendar.FEBRUARY, 15).getTime();
        Date spring18RegEnd = new GregorianCalendar(2018, Calendar.MARCH, 31).getTime();
        Quarter spring18 = new Quarter("spring18", spring18Start, spring18End, spring18RegStart, spring18RegEnd);
        // first add course to system
        Course mpcs51410 = alanAdmin.addCourseToSystem(Department.MPCS, 51410, spring18);
        // next create time blocks
        ArrayList<TimeBlock> meetingTimes = new ArrayList<TimeBlock>();
        TimeBlock tueEvening = new TimeBlock(DayOfWeek.TUESDAY, Time.valueOf("17:30:00"), Time.valueOf("20:30:00"));
        meetingTimes.add(tueEvening);
        alanAdmin.scheduleCourse(mpcs51410, meetingTimes);
        assertTrue(mpcs51410.getMeetingTimes().get(0).equals(tueEvening));
        // finally delete course from DB
        alanAdmin.deleteCourseFromSystem(mpcs51410);
    }

    // testing whether administrator
    // can add a room to the system
    @Test
    public void testAdminAddRoomToSystem(){
        System.out.println("Running testAdminAddRoomToSystem");
        // create administrator
        Administrator alanAdmin = new Administrator("Alan", "alan@uchicago.edu", 100);
        // create room
        Room addedRoom = alanAdmin.addRoomToSystem("young307", 50);
        Room roomInSystem = DatabaseConnectivity.getRoomByName("young307");
        assertTrue(addedRoom.equals(roomInSystem));
        // finally delete room from system
        DatabaseConnectivity.forceQuery("delete from room where name='" + roomInSystem.getName() +"'");
    }

    // testing whether administrator
    // can assign a room to a class
    @Test
    public void testAdminAssignRoom() {
        System.out.println("Running testAdminAssignRoom");
        // create administrator
        Administrator alanAdmin = new Administrator("Alan", "alan@uchicago.edu", 100);
        // create quarters
        Date spring18Start = new GregorianCalendar(2018, Calendar.APRIL, 1).getTime();
        Date spring18End = new GregorianCalendar(2018, Calendar.MAY, 31).getTime();
        Date spring18RegStart = new GregorianCalendar(2018, Calendar.FEBRUARY, 15).getTime();
        Date spring18RegEnd = new GregorianCalendar(2018, Calendar.MARCH, 31).getTime();
        Quarter spring18 = new Quarter("spring18", spring18Start, spring18End, spring18RegStart, spring18RegEnd);
        // first add course to system
        Course mpcs51410 = alanAdmin.addCourseToSystem(Department.MPCS, 51410, spring18);
        // then create room
        Room young306 = new Room("young306", 50);
        alanAdmin.assignCourseToRoom(mpcs51410, young306);
        assertTrue(mpcs51410.getRoom().equals(young306));
        alanAdmin.deleteCourseFromSystem(mpcs51410);
    }

    // testing whether an administrator
    // can modify the capacity of a room
    @Test
    public void testAdminSetRoomSize() {
        System.out.println("Running testAdminSetRoomSize");
        // create administrator
        Administrator alanAdmin = new Administrator("Alan", "alan@uchicago.edu", 100);
        // add room to system
        Room young307 = alanAdmin.addRoomToSystem("young307", 50);
        alanAdmin.setCapacity(young307, 100);
        assertEquals(young307.getCapacity(), 100);
        // delete room from system
        alanAdmin.deleteRoomFromSystem(young307);
    }

    // testing whether an administrator
    // can add a user to the system
    @Test
    public void testAddUserToSystem() {
        System.out.println("Running testAddStudentToSystem");
        // create administrator
        Administrator alanAdmin = new Administrator("Alan", "alan@uchicago.edu", 100);
        User jill = new Student("Jill", "jill@uchicago.edu", 420000);
        alanAdmin.addUserToSystem(jill);
        User expected = DatabaseConnectivity.getUserByEmail("jill@uchicago.edu");
        assertTrue(expected.getEmail().equals(jill.getEmail()));
        // force delete user
        DatabaseConnectivity.forceQuery("DELETE FROM user WHERE email='jill@uchicago.edu'");
        DatabaseConnectivity.forceQuery("DELETE FROM student WHERE email='jill@uchicago.edu'");
    }

    // test whether an administrator
    // can remove a user from the system
    @Test
    public void testRemoveUserFromSystem() {
        System.out.println("Running testRemoveUserFromSystem");
        // create administrator
        Administrator alanAdmin = new Administrator("Alan", "alan@uchicago.edu", 100);
        User fred = new Student("Fred", "fred@uchicago.edu", 190190);
        fred.writeToDB();
        User expected = alanAdmin.deleteUserFromSystem(fred);
        assertTrue(fred.getEmail().equals(expected.getEmail()));
    }

    // ********************** Student Tests ****************
    // *****************************************************

    // testing whether a student
    // can register for a course
    @Test
    public void testStudentAddCourse(){
        System.out.println("Running testStudentAddCourse");
        // create administrator and assign room to class
        Administrator alanAdmin = new Administrator("Alan", "alan@uchicago.edu", 100);
        // create quarters
        Date spring18Start = new GregorianCalendar(2018, Calendar.APRIL, 1).getTime();
        Date spring18End = new GregorianCalendar(2018, Calendar.MAY, 31).getTime();
        Date spring18RegStart = new GregorianCalendar(2018, Calendar.FEBRUARY, 15).getTime();
        Date spring18RegEnd = new GregorianCalendar(2018, Calendar.MARCH, 31).getTime();
        Quarter spring18 = new Quarter("spring18", spring18Start, spring18End, spring18RegStart, spring18RegEnd);
        // admin adds course to system
        Course math10001 = alanAdmin.addCourseToSystem(Department.MATH, 10001, spring18);
        Room young306 = new Room("young306", 250);
        // assign course to room
        alanAdmin.assignCourseToRoom(math10001, young306);
        // create new student
        Student jill = new Student("jill", "jill@uchicago.edu", 123555);
        alanAdmin.addUserToSystem(jill);
        // create enrollments
        // student tries to add course
        boolean addSuccessful = jill.addCourse(math10001);
        assertTrue(addSuccessful);
        alanAdmin.deleteCourseFromSystem(math10001);
        DatabaseConnectivity.forceQuery("DELETE FROM enrollment where alias='jill-MATH-10001-spring18'");
        alanAdmin.deleteCourseFromSystem(math10001);
        alanAdmin.deleteUserFromSystem(jill);
    }

    // testing whether a student
    // can drop a course
    @Test
    public void testStudentDropCourse() {
        System.out.println("Running testStudentDropCourse");
        Administrator alanAdmin = new Administrator("Alan", "alan@uchicago.edu", 100);
        // create quarters
        Date spring18Start = new GregorianCalendar(2018, Calendar.APRIL, 1).getTime();
        Date spring18End = new GregorianCalendar(2018, Calendar.MAY, 31).getTime();
        Date spring18RegStart = new GregorianCalendar(2018, Calendar.FEBRUARY, 15).getTime();
        Date spring18RegEnd = new GregorianCalendar(2018, Calendar.MARCH, 31).getTime();
        Quarter spring18 = new Quarter("spring18", spring18Start, spring18End, spring18RegStart, spring18RegEnd);
        // admin adds course to system
        Course math10001 = alanAdmin.addCourseToSystem(Department.MATH, 10001, spring18);
        Room young306 = new Room("young306", 250);
        // assign course to room
        alanAdmin.assignCourseToRoom(math10001, young306);
        // create new student
        Student jill = new Student("jill", "jill@uchicago.edu", 123555);
        alanAdmin.addUserToSystem(jill);
        // student tries to add course
        jill.addCourse(math10001);
        ArrayList<Enrollment> jillEnrollments = jill.getEnrollments();
        assertTrue(jillEnrollments.size() == 1);
        // student drops course
        jill.dropCourse(math10001);
        jillEnrollments = jill.getEnrollments();
        assertTrue(jillEnrollments.size() == 0);

        // remove student and course from system
        alanAdmin.deleteUserFromSystem(jill);
        alanAdmin.deleteCourseFromSystem(math10001);
    }

    // testing whether a student's attempt
    // to add a full course fails
    @Test
    public void testStudentAddFullCourse() {
        System.out.println("Running testStudentAddFullCourse");
        Administrator alanAdmin = new Administrator("Alan", "alan@uchicago.edu", 100);
        // create quarters
        Date spring18Start = new GregorianCalendar(2018, Calendar.APRIL, 1).getTime();
        Date spring18End = new GregorianCalendar(2018, Calendar.MAY, 31).getTime();
        Date spring18RegStart = new GregorianCalendar(2018, Calendar.FEBRUARY, 15).getTime();
        Date spring18RegEnd = new GregorianCalendar(2018, Calendar.MARCH, 31).getTime();
        Quarter spring18 = new Quarter("spring18", spring18Start, spring18End, spring18RegStart, spring18RegEnd);
        // admin adds course to system
        Course math10001 = alanAdmin.addCourseToSystem(Department.MATH, 10001, spring18);
        Room smallRoom = new Room("young306", 0);
        // assign course to room
        alanAdmin.assignCourseToRoom(math10001, smallRoom);
        // create new student
        Student jill = new Student("jill", "jill@uchicago.edu", 123555);
        alanAdmin.addUserToSystem(jill);
        // student tries to add course
        jill.addCourse(math10001);
        ArrayList<Enrollment> jillEnrollments = jill.getEnrollments();
        assertTrue(jillEnrollments.size() == 0);

        // remove student and course from system
        alanAdmin.deleteUserFromSystem(jill);
        alanAdmin.deleteCourseFromSystem(math10001);
    }

    // testing whether a student with restrictions
    // fails to add a course
    @Test
    public void testAddStudentWihRestrictionsToCourse() {
        System.out.println("Running testAddStudentWithRestrictionsToCourse");
        Administrator alanAdmin = new Administrator("Alan", "alan@uchicago.edu", 100);
        // create quarters
        Date spring18Start = new GregorianCalendar(2018, Calendar.APRIL, 1).getTime();
        Date spring18End = new GregorianCalendar(2018, Calendar.MAY, 31).getTime();
        Date spring18RegStart = new GregorianCalendar(2018, Calendar.FEBRUARY, 15).getTime();
        Date spring18RegEnd = new GregorianCalendar(2018, Calendar.MARCH, 31).getTime();
        Quarter spring18 = new Quarter("spring18", spring18Start, spring18End, spring18RegStart, spring18RegEnd);
        // admin adds course to system
        Course math10001 = alanAdmin.addCourseToSystem(Department.MATH, 10001, spring18);
        Room smallRoom = new Room("young306", 0);
        // assign course to room
        alanAdmin.assignCourseToRoom(math10001, smallRoom);
        // create new student
        Student jill = new Student("jill", "jill@uchicago.edu", 123555);
        alanAdmin.addUserToSystem(jill);
        alanAdmin.setStudentRestriction(jill, true);
        // student tries to add course
        jill.addCourse(math10001);
        ArrayList<Enrollment> jillEnrollments = jill.getEnrollments();
        assertTrue(jillEnrollments.size() == 0);
        // remove student and course from system
        alanAdmin.deleteUserFromSystem(jill);
        alanAdmin.deleteCourseFromSystem(math10001);
    }

    // testing whether a student
    // can view enrollments
    @Test
    public void testStudentViewEnrollments() {
        System.out.println("Running testStudentViewEnrollments");
        // create administrator and assign room to class
        Administrator alanAdmin = new Administrator("Alan", "alan@uchicago.edu", 100);
        // create quarters
        Date spring18Start = new GregorianCalendar(2018, Calendar.APRIL, 1).getTime();
        Date spring18End = new GregorianCalendar(2018, Calendar.MAY, 31).getTime();
        Date spring18RegStart = new GregorianCalendar(2018, Calendar.FEBRUARY, 15).getTime();
        Date spring18RegEnd = new GregorianCalendar(2018, Calendar.MARCH, 31).getTime();
        Quarter spring18 = new Quarter("spring18", spring18Start, spring18End, spring18RegStart, spring18RegEnd);
        // admin adds course to system
        Course math10001 = alanAdmin.addCourseToSystem(Department.MATH, 10001, spring18);
        Course mpcs54012 = alanAdmin.addCourseToSystem(Department.MPCS, 54012, spring18);
        Room young306 = new Room("young306", 250);
        // assign course to room
        alanAdmin.assignCourseToRoom(math10001, young306);
        alanAdmin.assignCourseToRoom(mpcs54012, young306);

        // create new student
        Student jill = new Student("jill", "jill@uchicago.edu", 123555);
        alanAdmin.addUserToSystem(jill);
        // student tries to add course
        jill.addCourse(math10001);
        jill.addCourse(mpcs54012);

        ArrayList<Enrollment> enrollments = new ArrayList<Enrollment>();
        enrollments.add(new Enrollment(math10001, jill, Grade.TBD));
        enrollments.add(new Enrollment(mpcs54012, jill, Grade.TBD));

        ArrayList<Enrollment> jillEnrollments = jill.getEnrollments();
        int i = 0;
        for (Enrollment e : jillEnrollments){
            assertTrue(e.equals(enrollments.get(i)));
            i++;
        }

        // remove test items from DB
        jill.dropCourse(math10001);
        jill.dropCourse(mpcs54012);
        alanAdmin.deleteCourseFromSystem(math10001);
        alanAdmin.deleteCourseFromSystem(mpcs54012);
        alanAdmin.deleteUserFromSystem(jill);
    }

    // ********* Faculty tests ***********
    // ***********************************

    //testing whether faculty
    // can assign a grade
    @Test
    public void testAssignGrade() {
        System.out.println("Running testAssignGrade");
        // create administrator and assign room to class
        Administrator alanAdmin = new Administrator("Alan", "alan@uchicago.edu", 100);
        // create quarters
        Date spring18Start = new GregorianCalendar(2018, Calendar.APRIL, 1).getTime();
        Date spring18End = new GregorianCalendar(2018, Calendar.MAY, 31).getTime();
        Date spring18RegStart = new GregorianCalendar(2018, Calendar.FEBRUARY, 15).getTime();
        Date spring18RegEnd = new GregorianCalendar(2018, Calendar.MARCH, 31).getTime();
        Quarter spring18 = new Quarter("spring18", spring18Start, spring18End, spring18RegStart, spring18RegEnd);
        // admin adds course to system
        Course math10001 = alanAdmin.addCourseToSystem(Department.MATH, 10001, spring18);
        Room young306 = new Room("young306", 250);
        // assign course to room
        alanAdmin.assignCourseToRoom(math10001, young306);
        // create new student
        Student jill = new Student("jill", "jill@uchicago.edu", 123555);
        alanAdmin.addUserToSystem(jill);
        // create enrollments
        // student tries to add course
        jill.addCourse(math10001);
        // create faculty
        Faculty martin = new Faculty("martin", "martin@uchicago.edu", 15000);
        martin.assignStudentGradeInCourse(jill, math10001, Grade.A);
        assertTrue(jill.getEnrollments().get(0).getGrade() == Grade.A);

        // delete test variables
        jill.dropCourse(math10001);
        alanAdmin.deleteCourseFromSystem(math10001);
        alanAdmin.deleteUserFromSystem(jill);
    }

    // ***** Database Connectivity tests ******
    // ****************************************
    /*
    @Test
    public void testGetAllCourses(){
        System.out.println("Running testGetAllCourses");
        Course first = new Course(Department.MPCS, 54300);
        ArrayList<Course> courses = DatabaseConnectivity.getAllCourses();
        System.out.println("COURSES IS " + courses);
        assertTrue(first.getCourseNumber() == courses.get(0).getCourseNumber() &&
                first.getDepartment() == courses.get(0).getDepartment());
    }

    @Test
    public void testGetUserWithEmail(){
        System.out.println("Test GetUserWithEmail (from DB)");
        Student expected = new Student("Sarah", "sarah@uchicago.edu", 123456);
        User sarah = DatabaseConnectivity.getUserByEmail("sarah@uchicago.edu");
        assertTrue(expected.getEmail().equals(sarah.getEmail()) &&
                expected.getName().equals(sarah.getName()) &&
                expected.getID() == sarah.getID() &&
                expected.getUsertype() == sarah.getUsertype());
    }
    */

    // ******* Tests Completed **********
    //***********************************
    @After
    public void testComplete() {
        System.out.println("Test complete");
    }
}
