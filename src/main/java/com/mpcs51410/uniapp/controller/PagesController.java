package com.mpcs51410.uniapp.controller;
import com.mpcs51410.uniapp.model.*;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.stream.Stream;
import java.util.stream.Collectors;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.ModelMap;

import javax.servlet.RequestDispatcher;

@Controller
public class PagesController {

    @RequestMapping("/")
    public String listCourses(ModelMap modelMap){
        ArrayList<Course> courses = DatabaseConnectivity.getAllCourses();
        modelMap.put("courses", courses);
        return "home";
    }

    @RequestMapping("/login")
    public String login(){
        return "login";
    }

    @RequestMapping("/signin")
    public String signin(@RequestParam("email") String email){
        // check that e-mail exists in the database
        User user = DatabaseConnectivity.getUserByEmail(email);
        if(user == null) return "login";
        // if e-mail exists, render use page
        String[] emailParts = email.split("@");
        return "redirect:/user/" + emailParts[0] + "/home";
    }

    @RequestMapping("/user/{emailPrefix}/home")
    public String signinSuccess(@PathVariable String emailPrefix, ModelMap modelMap){
        System.out.println("Searching for user with e-mail: " + emailPrefix + "@uchicago.edu");
        // get user with given email prefix
        User user = DatabaseConnectivity.getUserByEmail(emailPrefix + "@uchicago.edu");
        modelMap.put("user", user);
        // get list of courses offered
        ArrayList<Course> courses = DatabaseConnectivity.getAllCourses();
        modelMap.put("courses", courses);
        modelMap.addAttribute("emailprefix", emailPrefix);

        // get number of enrollments in each class
        HashMap<Course, Integer> numEnrollments = new HashMap<>();
        for (Course course : courses){
            Integer num = new Integer(course.getRoster().size());
            numEnrollments.put(course, num);
        }
        modelMap.put("numEnrollments", numEnrollments);
        switch (user.getUsertype()){
            case STUDENT: return "student/home";
            case FACULTY: return "faculty/home";
            case ADMINISTRATOR: return "administrator/home";
            default: return "404";
        }
    }

    // *************************
    // Administrator Controllers
    // *************************

    @RequestMapping(value = "/user/{emailPrefix}/administrator/courses")
    public String adminViewCourses(ModelMap modelMap, @PathVariable String emailPrefix){
        User user = DatabaseConnectivity.getUserByEmail(emailPrefix + "@uchicago.edu");
        Administrator admin = (Administrator) user;
        ArrayList<Course> courses = DatabaseConnectivity.getAllCourses();
        modelMap.put("courses", courses);
        modelMap.addAttribute("emailprefix", emailPrefix);
        return "administrator/courses";
    }

    @RequestMapping(value = "/user/{emailPrefix}/administrator/editcourse/{courseAlias}")
    public String adminEditCourse(ModelMap modelMap, @PathVariable String emailPrefix,
                                  @PathVariable String courseAlias){
        User user = DatabaseConnectivity.getUserByEmail(emailPrefix + "@uchicago.edu");
        Administrator admin = (Administrator) user;
        Course course = DatabaseConnectivity.getCourseByAlias(courseAlias);
        modelMap.put("course", course);
        modelMap.put("department", course.getDepartment().name());
        modelMap.addAttribute("emailprefix", emailPrefix);
        // retrieve all faculty members and put them in modelMap
        ArrayList<User> facultyMembers = DatabaseConnectivity.getAllUsersGivenUserType("FACULTY");
        modelMap.put("facultyMembers", facultyMembers);
        // retrieve all rooms and put them in the modelMap
        ArrayList<Room> rooms = DatabaseConnectivity.getAllRooms();
        modelMap.put("rooms", rooms);
        return "administrator/editcourse";
    }

    @RequestMapping(value = "/user/{emailPrefix}/administrator/editcourse/{courseAlias}/submit", method= RequestMethod.POST)
    public String adminSubmitCourseEdits(ModelMap modelMap, @PathVariable String courseAlias,
                                @RequestParam(value="faculty") String facultyEmail,
                                @RequestParam(value="room") String roomName,
                                @RequestParam(value="isopen") String isOpen){
        System.out.println("Editing a course");
        System.out.println("faculty's email is: " + facultyEmail);
        System.out.println("room is: " + roomName);
        System.out.println("isOpen is: " + isOpen);
        Course course = DatabaseConnectivity.getCourseByAlias(courseAlias);
        // write query to update entry in the database
        String query = "UPDATE course";
        // is open
        if(isOpen.equals("true")) query += " SET isOpen=1";
        else query += " SET isOpen=0";
        // faculty
        if (!facultyEmail.equals("")) query+= ", facultyEmail='" + facultyEmail +"'";
        else query+= ", facultyEmail=NULL";
        // room
        if (!roomName.equals("")) query+= ", roomName='" + roomName + "'";
        else query+= ", roomName=NULL";
        // where...
        query += " WHERE alias='" + courseAlias + "'";
        DatabaseConnectivity.forceQuery(query);
        return "redirect:/user/{emailPrefix}/administrator/courses";
    }

    // delete course
    @RequestMapping(value = "/user/{emailPrefix}/administrator/deletecourse/{courseAlias}")
    public String adminSubmitCourseEdits(ModelMap modelMap, @PathVariable String courseAlias){
        Course course = DatabaseConnectivity.getCourseByAlias(courseAlias);
        course.deleteFromDB();
        return "redirect:/user/{emailPrefix}/administrator/courses";
    }

    // new user form
    @RequestMapping("/user/{emailPrefix}/administrator/newuser")
    public String addUserForm(@PathVariable String emailPrefix, ModelMap modelMap){
        modelMap.addAttribute("emailprefix", emailPrefix);
        return "administrator/newuser";
    }
    // create new student
    @RequestMapping(value = "/user/{emailPrefix}/administrator/createstudent", method= RequestMethod.POST)
    public String createStudent(ModelMap modelMap, @PathVariable String emailPrefix,
                                @RequestParam(value="name") String name,
                                @RequestParam(value="email") String email,
                                @RequestParam(value="id") int id){
        System.out.println("Creating a new student");
        System.out.println("name is: " + name);
        System.out.println("email is: " + email);
        System.out.println("id is: " + id);
        User newStudent = new Student(name, email, id);
        newStudent.writeToDB();
        modelMap.addAttribute("emailprefix", emailPrefix);
        return "redirect:/user/{emailPrefix}/home";
    }

    @RequestMapping("/user/{emailPrefix}/administrator/newcourse")
    public String addCourseForm(@PathVariable String emailPrefix, ModelMap modelMap){

        ArrayList<Quarter> quarters = DatabaseConnectivity.getCurrentFutureQuarters();
        modelMap.put("quarters", quarters);

        List<String> departments = Stream.of(Department.values())
                .map(Department::name)
                .collect(Collectors.toList());
        modelMap.put("departments", departments);
        modelMap.addAttribute("emailprefix", emailPrefix);
        return "administrator/newcourse";
    }

    @RequestMapping(value = "/user/{emailPrefix}/administrator/createcourse", method= RequestMethod.POST)
    public String createCourse(ModelMap modelMap,
                                @RequestParam(value="department") String department,
                                @RequestParam(value="courseNum") String courseNum,
                                @RequestParam(value="quarter") String quarterAlias){
        Quarter quarter = DatabaseConnectivity.getQuarterByAlias(quarterAlias);
        Course course = new Course(Department.valueOf(department), Integer.parseInt(courseNum), quarter);
        course.writeToDB();
        return "redirect:/user/{emailPrefix}/administrator/courses";
    }

    @RequestMapping(value = "/user/{emailPrefix}/administrator/viewstudents")
    public String adminViewStudents(ModelMap modelMap, @PathVariable String emailPrefix){
        ArrayList<User> students = DatabaseConnectivity.getAllUsersGivenUserType(UserType.STUDENT.name());
        modelMap.put("students", students);
        HashMap<String, Boolean> hasRestrictionsMap = new HashMap<>();
        for (User student : students){
            Boolean bool = DatabaseConnectivity.getIfStudentHasRestrictionByEmail(student.getEmail());
            hasRestrictionsMap.put(student.getEmail(), bool);
        }
        modelMap.put("hasRestrictions", hasRestrictionsMap);
        modelMap.put("emailprefix", emailPrefix);
        return "administrator/students";
    }

    @RequestMapping(value = "/user/{emailPrefix}/administrator/changerestriction/{studentid}")
    public String adminViewStudents(ModelMap modelMap, @PathVariable int studentid){
        User user = DatabaseConnectivity.getUserGivenID(studentid);
        Student student = (Student) user;
        if (DatabaseConnectivity.getIfStudentHasRestrictionByEmail(user.getEmail()) == true)
            student.setHasRestriction(false);
        else student.setHasRestriction(true);
        return "redirect:/user/{emailPrefix}/administrator/viewstudents";
    }

    @RequestMapping(value = "/user/{emailPrefix}/administrator/delete/{id}")
    public String adminDeleteStudents(ModelMap modelMap, @PathVariable int id){
        User user = DatabaseConnectivity.getUserGivenID(id);
        user.deleteFromDB();
        if (user.getUsertype() == UserType.STUDENT)
            return "redirect:/user/{emailPrefix}/administrator/viewstudents";
        if (user.getUsertype() == UserType.FACULTY)
            return "redirect:/user/{emailPrefix}/administrator/viewfaculty";
        else
            return "redirect:/user/{emailPrefix}/home";
    }

    @RequestMapping(value = "/user/{emailPrefix}/administrator/viewfaculty")
    public String adminViewFaculty(ModelMap modelMap, @PathVariable String emailPrefix){
        ArrayList<User> facultyMembers = DatabaseConnectivity.getAllUsersGivenUserType(UserType.FACULTY.name());
        modelMap.put("facultyMembers", facultyMembers);
        modelMap.put("emailprefix", emailPrefix);
        return "administrator/faculty";
    }

    // ********************
    // Student Controllers
    // ********************


    @RequestMapping("/user/{emailPrefix}/student/courses")
    public String studentCoursePage(@PathVariable String emailPrefix, ModelMap modelMap){
        // return courses in which student is enrolled
        User user = DatabaseConnectivity.getUserByEmail(emailPrefix + "@uchicago.edu");
        Student student = (Student) user;
        ArrayList<Enrollment> enrollments = student.getEnrollments();
        modelMap.put("enrollments", enrollments);
        modelMap.addAttribute("emailprefix", emailPrefix);
        return "student/courses";
    }


    // controller for signing up for course
    @RequestMapping(value = "/user/{emailPrefix}/student/signup/{courseAlias}")
    public String studentSignUpForCourse(@PathVariable String emailPrefix,
                                         @PathVariable String courseAlias, ModelMap modelMap){
        // get student from URL
        User user = DatabaseConnectivity.getUserByEmail(emailPrefix + "@uchicago.edu");
        Student student = (Student) user;
        student.loadEnrollments();
        // get course from URL
        Course course = DatabaseConnectivity.getCourseByAlias(courseAlias);
        System.out.println("course is: " + course.getAlias());
        boolean isAddSuccessful = student.addCourse(course);
        System.out.println("isAddSuccessful is " + isAddSuccessful);
        return "redirect:/user/{emailPrefix}/student/courses";
    }

    // controller for dropping courses
    @RequestMapping(value = "/user/{emailPrefix}/student/drop/{courseAlias}")
    public String studentDropCourse(@PathVariable String emailPrefix,
                                         @PathVariable String courseAlias, ModelMap modelMap){
        // get student from URL
        User user = DatabaseConnectivity.getUserByEmail(emailPrefix + "@uchicago.edu");
        Student student = (Student) user;
        System.out.println("student is: " + student.getEmail());
        // get course from URL
        Course course = DatabaseConnectivity.getCourseByAlias(courseAlias);
        course.loadRoster();
        student.dropCourse(course);
        System.out.println("course is: " + course.getAlias());
        boolean isDropSuccessful = student.dropCourse(course);
        System.out.println("isDropSuccessful: " + isDropSuccessful);
        return "redirect:/user/{emailPrefix}/student/courses";
    }

    @RequestMapping("/user/{emailPrefix}/student/profile")
    public String studentProfilePage(@PathVariable String emailPrefix, ModelMap modelMap){
        User user = DatabaseConnectivity.getUserByEmail(emailPrefix + "@uchicago.edu");
        modelMap.addAttribute("emailprefix", emailPrefix);
        modelMap.put("user", user);
        return "student/profile";
    }

    // ********************
    // Faculty Controllers
    // ********************

    @RequestMapping("/user/{emailPrefix}/faculty/courses")
    public String facultyCoursesPage(@PathVariable String emailPrefix, ModelMap modelMap){
        User user = DatabaseConnectivity.getUserByEmail(emailPrefix + "@uchicago.edu");
        Faculty faculty = (Faculty) user;
        ArrayList<Course> courses = faculty.getCourses();
        modelMap.addAttribute("emailprefix", emailPrefix);
        modelMap.put("user", user);
        modelMap.put("faculty", faculty);
        modelMap.put("courses", courses);
        return "faculty/courses";
    }

    @RequestMapping("/user/{emailPrefix}/faculty/assigngrades/{alias}")
    public String facultyAssignGrades(@PathVariable String emailPrefix, @PathVariable String alias, ModelMap modelMap){
        // get faculty
        User user = DatabaseConnectivity.getUserByEmail(emailPrefix + "@uchicago.edu");
        Faculty faculty = (Faculty) user;
        // get students in given course
        Course course = DatabaseConnectivity.getCourseByAlias(alias);
        ArrayList<Student> roster = course.getRoster();

        // get student's grades and store them in a hashtable
        HashMap<Student, Grade> studentgrade = new HashMap<>();
        for (Student student : roster){
            Enrollment enrollment = student.getEnrollmentGivenCourseAlias(alias);
            studentgrade.put(student, enrollment.getGrade());
        }
        modelMap.put("studentgrade", studentgrade);
        modelMap.addAttribute("emailprefix", emailPrefix);
        modelMap.put("user", user);
        modelMap.put("faculty", faculty);
        modelMap.put("course", course);
        modelMap.put("roster", roster);
        return "faculty/assigngrades";
    }

    // controller for submitting grades
    @RequestMapping(value = "/user/{emailPrefix}/faculty/submitgrades/{alias}/{studentid}")
    public String facultySubmitGrades(@RequestParam("grade") String grade, @PathVariable String emailPrefix,
                                         @PathVariable String alias, @PathVariable int studentid, ModelMap modelMap){
        // get faculty and course from URL
        User user = DatabaseConnectivity.getUserByEmail(emailPrefix + "@uchicago.edu");
        Faculty faculty = (Faculty) user;
        Course course = DatabaseConnectivity.getCourseByAlias(alias);
        User studentuser = DatabaseConnectivity.getUserGivenID(studentid);
        Student student = (Student) studentuser;
        // assign grade
        faculty.assignStudentGradeInCourse(student, course, Grade.valueOf(grade));
        return "redirect:/user/{emailPrefix}/faculty/assigngrades/{alias}";
    }

    @RequestMapping("/user/{emailPrefix}/faculty/profile")
    public String facultyProfilePage(@PathVariable String emailPrefix, ModelMap modelMap){
        User user = DatabaseConnectivity.getUserByEmail(emailPrefix + "@uchicago.edu");
        modelMap.addAttribute("emailprefix", emailPrefix);
        modelMap.put("user", user);
        return "faculty/profile";
    }




}
