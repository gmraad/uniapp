package com.mpcs51410.uniapp.model;
import java.util.Date;

public class Quarter {
	private int quarterID;
	private String alias;
	private Date startDate;
	private Date endDate;
	private Date regPeriodStart;
	private Date regPeriodEnd;

	public Quarter(String alias, Date startDate, Date endDate, Date regPeriodStart, Date regPeriodEnd) {
		if(alias == null || startDate == null || endDate == null || regPeriodStart == null || regPeriodEnd == null) {
			throw new RuntimeException("Provide a start and end date for the quarter, "
					+ "as well as a start and end date for the registration period");
		}
		this.alias = alias;
		this.startDate = startDate;
		this.endDate = endDate;
		this.regPeriodStart = regPeriodStart;
		this.regPeriodEnd = regPeriodEnd;
		this.quarterID = 0; // set quarter id
	}
	
	// check if registration period is still on
	// i.e. check if today is within the start and end of the registration period
	// return true if it is; false otherwise
	public boolean isRegistrationPeriodOn() {
		Date today = new Date();
		if(today.after(regPeriodStart) && today.before(regPeriodEnd))
			return true;
		else return false;
	}
	
	// getter methods
	public String getAlias(){ return this.alias; }

	public int getQuarterID() {
		return this.quarterID;
	}
	
	public Date getStartDate() {
		return this.startDate;
	}
	
	public Date getEndDate() {
		return this.endDate;
	}
}
