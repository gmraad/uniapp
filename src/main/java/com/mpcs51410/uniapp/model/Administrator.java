package com.mpcs51410.uniapp.model;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Administrator extends User {
	public Administrator(String name, String email, int id) {
		// construct User
		super(name, email, id);
		// create administrator object in the database
	}
	
	// Course Management
	// **************************************
	
	// add course to system
	// return course if successful and null if unsuccessful
	// note course constructor may also throw exception
	public Course addCourseToSystem(Department department, int courseNum, Quarter quarter) {
		Course course = new Course(department, courseNum, quarter);
		course.writeToDB();
		return course;
	}
	
	// delete course from system
	// return deleted course if successful
	// return null otherwise
	public Course deleteCourseFromSystem(Course course) {
		course.deleteFromDB();
		return course;
	}

	// set whether given course is open or closed
	public void setIfCourseIsOpen(Course course, boolean isOpen) {
		course.setIsOpen(isOpen);
	}
	
	// open or close all courses
	public void setIfCourseIsOpenAllCourses(boolean isOpen) {
		Connection conn = DatabaseConnectivity.getConnection();
		Statement stmt = DatabaseConnectivity.getStatement(conn);
		ResultSet rs = null;
		// gather list of courses
		try {
			String alias = null;
			rs = stmt.executeQuery("SELECT * FROM course"); 
			while (rs.next()) {
		            alias = rs.getString("alias");
		            PreparedStatement updateCourseIsOpen = conn.prepareStatement("UPDATE course SET isOpen = ? WHERE alias = ?");
		            updateCourseIsOpen.setBoolean(1, isOpen);
		            updateCourseIsOpen.setString(2, alias);
		            updateCourseIsOpen.executeUpdate();
		    }
		} catch(SQLException e) {
		    		e.printStackTrace();
	    }
		
	}
	
	// assign given class to given room
	public void assignCourseToRoom(Course course, Room room) {
		course.setRoom(room);
	}
	
	// schedule course
	public void scheduleCourse(Course course, ArrayList<TimeBlock> meetingTimes) {
		course.setMeetingTimes(meetingTimes);
	}
	
	// Room Management 
	// ***************************************
	
	// create new rooms in the system
	public Room addRoomToSystem(String name, int capacity) {
		Room room =new Room(name, capacity);
		room.writeToDB();
		return room;
	}
	
	// assign capacity to a room
	public void setCapacity(Room room, int capacity) {
		room.setCapacity(capacity);
	}

	public Room deleteRoomFromSystem(Room room){
		room.deleteFromDB();
		return room;
	}

	// User Management
	// **************************

	// adds user to the database
	public User addUserToSystem(User user) {
		user.writeToDB();
		return user;
	}
	
	// delete user from system
	// return deleted user if successful
	// return null if user cannot be found
	public User deleteUserFromSystem(User user) {
		User u = DatabaseConnectivity.getUserByEmail(user.getEmail());
		u.deleteFromDB();
		return u;
	}
	
	public void setStudentRestriction(Student student, boolean bool) {
		student.setHasRestriction(bool);
	}
}
