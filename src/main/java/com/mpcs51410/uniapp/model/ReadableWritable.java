package com.mpcs51410.uniapp.model;

public interface ReadableWritable {
	public void writeToDB();
	public ReadableWritable readFromDB(int id);
	public void deleteFromDB();

}
