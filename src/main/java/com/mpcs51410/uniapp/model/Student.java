package com.mpcs51410.uniapp.model;
import java.util.ArrayList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Student extends User {
	// instance variables
	private boolean hasRestriction;
	private ArrayList<Enrollment> enrollments;
	
	// constructor method
	public Student(String name, String email, int id) {
		super(name, email, id);
		this.hasRestriction = false;
		this.enrollments = new ArrayList<Enrollment>();
	}	
	
	// add Course to student's account
	// returns true if add was successful
	// returns false if student is already enrolled in course
	public boolean addCourse(Course course) {
		if(this.hasRestriction == true) return false;
		RegistrarForm form = new RegistrarForm(this, course, Action.ADD);
		if (form.getIsActionSuccessful()){
			Enrollment enrollment = new Enrollment(course, this, Grade.TBD);
			enrollment.writeToDB();
			return true;
		} else return false;
	}
	
	// drop Course from student's account
	// return true if drop was successful
	// returns false if student was not enrolled in course
	public boolean dropCourse(Course course) {
		RegistrarForm form = new RegistrarForm(this, course, Action.DROP);
		if (form.getIsActionSuccessful()){
			Enrollment e = studentEnrollmentInCourse(course);
			this.enrollments.remove(e);
			DatabaseConnectivity.forceQuery("DELETE FROM enrollment WHERE alias='" + e.getAlias() + "';");
			return true;
		}
		return false;
	}
	
	
	// checks if student is enrolled in course
	// returns Enrollment object if student is enrolled; null otherwise
	public Enrollment studentEnrollmentInCourse(Course course) {
		ArrayList<Enrollment> enrollments = this.getEnrollments();
		for(Enrollment e : enrollments) {
			// check for literal equality
			if(e.getCourse().equals(course)) return e;
		}
		return null;
	}
	
	// set restrictions on student's account
	public void setHasRestriction(boolean bool) {
		this.hasRestriction = bool;
		DatabaseConnectivity.forceQuery("UPDATE student SET hasRestrictions="+ bool + " WHERE email='" + getEmail() + "';");
	}
	
	// check if student has reached enrollment limit
	// e.g. return true if student is in more than 3 courses
	public boolean hasReachedEnrollmentLimit() {
		if (this.getEnrollments().size() >= 3) return true;
		else return false;
	}


	public Enrollment getEnrollmentGivenCourseAlias(String alias){
		ArrayList<Enrollment> enrollments = this.getEnrollments();
		for (Enrollment enrollment : enrollments){
			if (enrollment.getCourse().getAlias().equals(alias))
				return enrollment;
		}
		return null;
	}
	
	// getter methods
	// ****************
	public boolean hasRestriction() {
		this.hasRestriction = DatabaseConnectivity.getIfStudentHasRestrictionByEmail(this.getEmail());
		return this.hasRestriction;
	}
	
	
	public ArrayList<Enrollment> fetchEnrollments(){
		// pull enrollments from 
		Connection conn = DatabaseConnectivity.getConnection();
		Statement stmt = DatabaseConnectivity.getStatement(conn);
		ResultSet rs = null;
		ArrayList<Enrollment> enrollments = new ArrayList<Enrollment>();
		Enrollment enrollment;
		try {
			rs = stmt.executeQuery("SELECT * FROM enrollment WHERE studentEmail='" + this.getEmail()+"';");
		    while (rs.next()) {
				String courseAlias = rs.getString("courseAlias");
				String grade = rs.getString("grade");
				Course course = DatabaseConnectivity.getCourseByAlias(courseAlias);
				enrollment = new Enrollment(course, this, Grade.valueOf(grade));
				enrollments.add(enrollment);
		     }	 
		 } catch (Exception e) {
			 	e.printStackTrace();
		 } finally {
			 	try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			 	DatabaseConnectivity.close(conn, stmt);
		 }
		return enrollments;
	}

	public void loadEnrollments(){
		this.enrollments = fetchEnrollments();
	}

	public ArrayList<Enrollment> getEnrollments(){
		if (enrollments == null || enrollments.size() == 0)
			this.enrollments = fetchEnrollments();
		return this.enrollments;
	}
}
