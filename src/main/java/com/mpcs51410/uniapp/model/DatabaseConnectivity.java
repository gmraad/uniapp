package com.mpcs51410.uniapp.model;
import java.util.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class DatabaseConnectivity {
	
	public static Connection getConnection() {
		 Connection conn = null;
		 try {
			 // new com.mysql.jdbc.Driver();
			 Class.forName("com.mysql.jdbc.Driver").newInstance();
			 // to start a database in terminal
			 // 1) mysql -u root -p (use password below) 
			 // 2) "use "regsystem"
			 // 3) SHOW TABLES; # to check that db is connected
			 String connectionUrl = "jdbc:mysql://127.0.0.1:3306/regsystem?useSSL=false";
             String connectionUser = "root";
             String connectionPassword = "password";
             conn = DriverManager.getConnection(connectionUrl, connectionUser, connectionPassword);
             return conn;
		 } catch (Exception e) {
			 e.printStackTrace();
		 }
		 return conn;
	}
	
	public static Statement getStatement (Connection conn) {
         Statement stmt = null;
         try {
                 stmt = conn.createStatement();
                 return stmt;
         } catch (Exception e) {
                 e.printStackTrace();
         }
         return stmt;
	 }
	 
	 public static void close(Connection conn, Statement stmt) {
		 try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
         try { if (conn != null) conn.close(); } catch (SQLException e) { e.printStackTrace(); }
	 }

	 // User Management Database Methods
	 // ********************************
	// View ALL users

	// CREATE
	// **************
	public static void writeUserToDB(User user){
		// create user object in the database
		Connection conn = DatabaseConnectivity.getConnection();
		Statement stmt = getStatement(conn);
		ResultSet rs = null;
		try {
			// create student user
			PreparedStatement createUser = conn.prepareStatement("INSERT INTO user VALUES (?, ?, ?, ?)");
			createUser.setInt(1, user.getID());
			createUser.setString(2, user.getName());
			createUser.setString(3, user.getEmail());
			createUser.setString(4, user.getUsertype().toString().toUpperCase());
			createUser.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			close(conn, stmt);
		}
	}

	// READ
	// **************
	// Read All users
	// If want all users irrespetive of type, set userType to null
	public static ArrayList<User> getAllUsersGivenUserType(String userType) {
		 Connection conn = getConnection();
		 Statement stmt = getStatement(conn);
		 ResultSet rs = null;
		 ArrayList<User> users = new ArrayList<User>();
		 // gather list of users saved in database
	     // table: user
		 try {
		 	// choose query to execute
		 	 if (userType == null) rs = stmt.executeQuery("SELECT * FROM user");
		 	 if (userType.toUpperCase().equals("STUDENT"))
		 	 	rs = stmt.executeQuery("SELECT * FROM user where usertype='STUDENT'");
			 if (userType.toUpperCase().equals("FACULTY"))
				 rs = stmt.executeQuery("SELECT * FROM user where usertype='FACULTY'");
			 if (userType.toUpperCase().equals("ADMINISTRATOR"))
				 rs = stmt.executeQuery("SELECT * FROM user where usertype='ADMINISTRATOR'");
		     // iterate through results
			 while (rs.next()) {
				 int id = rs.getInt(1);
				 String username = rs.getString("name");
				 String useremail = rs.getString("email");
				 String usertype = rs.getString("usertype");
				 if (usertype.toUpperCase().equals("STUDENT")) users.add(new Student(username, useremail, id));
				 if (usertype.toUpperCase().equals("FACULTY")) users.add(new Faculty(username, useremail, id));
				 if (usertype.toUpperCase().equals("ADMINISTRATOR")) users.add(new Administrator(username, useremail, id));
		     }
		 } catch (Exception e) {
             e.printStackTrace();
		 } finally {
			 try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			 close(conn, stmt);
		 }	 
		 return users;
	}

	public static boolean getIfStudentHasRestrictionByEmail(String email){
		Connection conn = getConnection();
		Statement stmt = getStatement(conn);
		ResultSet rs = null;
		int hasRestrictions = 0;
		try {
			// choose query to execute
			rs = stmt.executeQuery("SELECT * FROM student WHERE email='" + email +"';");
			// iterate through results
			while (rs.next()) {
				hasRestrictions = rs.getInt("hasRestrictions");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			close(conn, stmt);
		}
		boolean result = (hasRestrictions == 1) ? true : false;
		return result;
	}

	// Read user given id
	public static User getUserGivenID(int id) {
		User result = null;
		Connection conn = getConnection();
		Statement stmt = getStatement(conn);
		ResultSet rs = null;
		// gather list of users saved in database
		// table: user
		try {
			rs = stmt.executeQuery("SELECT * FROM user WHERE id=" + id);
			int userid = 0;
			String username = null;
			String useremail = null;
			String usertype = null;
			int count = 0; // use to check if result is null
			while (rs.next()) {
				userid = rs.getInt(1);
				username = rs.getString("name");
				useremail = rs.getString("email");
				usertype = rs.getString("usertype");
				count++;
			}
			if (count == 0) return result;
			if (usertype.equals("Student".toUpperCase())) result = new Student(username, useremail, userid);
			if (usertype.equals("Faculty".toUpperCase())) result = new Faculty(username, useremail, userid);
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			close(conn, stmt);
		}
		return result;
	}

	// Reader user given email
	public static User getUserByEmail(String email){
		Connection conn = getConnection();
		Statement stmt = getStatement(conn);
		ResultSet rs = null;
		User user = null;
		String name = null;
		String userType = null;
		int id = -1;
		try {
			rs = stmt.executeQuery("SELECT * FROM user WHERE email='" + email +"'");
			while (rs.next()) {
				id = rs.getInt(1);
				name = rs.getString("name");
				userType = rs.getString("usertype");
			}
			if(name == null) return user; // user not found
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			close(conn, stmt);
		}
		// otherwise create and return user object
		if(userType.toUpperCase().equals("STUDENT")) user = new Student(name, email, id);
		if(userType.toUpperCase().equals("FACULTY")) user = new Faculty(name, email, id);
		if(userType.toUpperCase().equals("ADMINISTRATOR")) user = new Administrator(name, email, id);
		return user;
	}

	// *******************************
	// Course Management Database Methods
	//********************************

	// CREATE COURSE
	// **************
	public static void writeCourseToDB(Course course) {
		// create user object in the database
		Connection conn = DatabaseConnectivity.getConnection();
		Statement stmt = getStatement(conn);
		ResultSet rs = null;
		try {
			// create student user
			PreparedStatement createCourse = conn.prepareStatement("INSERT INTO course VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
			createCourse.setInt(1, course.getID());
			createCourse.setString(2, course.getDepartment().toString());
			createCourse.setInt(3, course.getCourseNumber());
			createCourse.setString(4, course.getQuarter().getAlias());
			// check if faculty is assigned
			String facultyEmail = (course.getFaculty() == null) ? null : course.getFaculty().getEmail();
			createCourse.setString(5, facultyEmail);
			// check if room is assigned
			String roomName = (course.getRoom() == null) ? null : course.getRoom().getName();
			createCourse.setString(6, roomName);

			createCourse.setBoolean(7, course.getIsOpen());
			createCourse.setString(8, course.getAlias());
			createCourse.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			close(conn, stmt);
		}
	}

	// READ
	// ***************
	// View ALL courses offered
	 public static ArrayList<Course> getAllCourses() {
		 Connection conn = getConnection();
		 Statement stmt = getStatement(conn);
		 ResultSet rs = null;
		 ArrayList<Course> courses = new ArrayList<Course>();
		 Room room = null;
		 // gather list of users saved in database
		 // table: user
		 try {
			 rs = stmt.executeQuery("SELECT * FROM course");
			 while (rs.next()) {
				int id = rs.getInt(1);
				String department = rs.getString("department");
				int courseNum = rs.getInt("courseNum");
				String quarterAlias = rs.getString("quarter");
				Quarter quarter = getQuarterByAlias(quarterAlias);
				Course course = new Course(Department.valueOf(department.toUpperCase()), courseNum, quarter);
				String facultyEmail = rs.getString("facultyEmail");
				User faculty = getUserByEmail(facultyEmail);
				if (faculty != null) course.setFaculty((Faculty) faculty);
				String roomName = rs.getString("roomName");
				if (roomName != null) room = getRoomByName(roomName);
				if (room != null) course.setRoom(room);
				boolean isOpen = rs.getBoolean("isOpen");
				course.setIsOpen(isOpen);
				course.updateAlias();
				courses.add(course);
			 }
		 } catch (Exception e) {
			 e.printStackTrace();
		 } finally {
			 try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			 close(conn, stmt);
		 }
		 return courses;
	 }

	// Read course given alias
	public static Course getCourseByAlias(String alias){
		Connection conn = getConnection();
		Statement stmt = getStatement(conn);
		ResultSet rs = null;
		Course course = null;
		try {
			rs = stmt.executeQuery("SELECT * FROM course WHERE alias='" + alias +"'");
			int id = 0;
			Department department = null;
			int courseNum = -1;
			Quarter quarter = null;
			Room room = null;
			String userType = null;
			while (rs.next()) {
				id = rs.getInt(1);
				department = Department.valueOf(rs.getString("department"));
				courseNum = rs.getInt("courseNum");

				course = new Course(department, courseNum);

				String quarterAlias = rs.getString("quarter");
				if (quarterAlias != null) quarter = getQuarterByAlias(quarterAlias);
				if (quarter != null) course.setQuarter(quarter);


				String facultyEmail = rs.getString("facultyEmail");
				User faculty = getUserByEmail(facultyEmail);
				if (faculty != null) course.setFaculty((Faculty) faculty);


				String roomName = rs.getString("roomName");
				if (roomName != null) room = getRoomByName(roomName);
				if (room != null) course.setRoom(room);

				boolean isOpen = rs.getBoolean("isOpen");
				course.setIsOpen(isOpen);

				course.updateAlias();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			close(conn, stmt);
		}
		return course;
	}

	// Read course given id
	public static Course getCourseGivenID(int id) {
		Connection conn = getConnection();
		Statement stmt = getStatement(conn);
		ResultSet rs = null;
		Course course = null;
		try {
			rs = stmt.executeQuery("SELECT * FROM course WHERE id='" + id + "'");
			Department department = null;
			int courseNum = -1;
			Quarter quarter = null;
			Room room = null;
			String userType = null;
			while (rs.next()) {
				id = rs.getInt(1);
				department = Department.valueOf(rs.getString("department"));
				courseNum = rs.getInt("courseNum");

				course = new Course(department, courseNum);

				String quarterAlias = rs.getString("quarter");
				if (quarterAlias != null) quarter = getQuarterByAlias(quarterAlias);
				if (quarter != null) course.setQuarter(quarter);


				String facultyEmail = rs.getString("facultyEmail");
				User faculty = getUserByEmail(facultyEmail);
				if (faculty != null) course.setFaculty((Faculty) faculty);


				String roomName = rs.getString("roomName");
				if (roomName != null) room = getRoomByName(roomName);
				if (room != null) course.setRoom(room);

				boolean isOpen = rs.getBoolean("isOpen");
				course.setIsOpen(isOpen);

				course.updateAlias();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			close(conn, stmt);
		}
		return course;
	}

	public static ArrayList<Student> getCourseRosterByAlias (String alias){
		Connection conn = getConnection();
		Statement stmt = getStatement(conn);
		ResultSet rs = null;
		ArrayList<Student> roster = new ArrayList<Student>();
		String studentEmail;
		Student student;
		try {
			rs = stmt.executeQuery("SELECT * FROM enrollment WHERE courseAlias='" + alias +"'");
			while (rs.next()) {
				studentEmail = rs.getString("studentEmail");
				User user  = getUserByEmail(studentEmail);
				student = (Student) user;
				if (student == null) break;
				roster.add(student);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			close(conn, stmt);
		}
		return roster;

	}

	// DELETE COURSE
	// ********
	public static Course deleteCourse(Course course) {
		if (course == null) return null; // user with given id does not exist
		Connection conn = DatabaseConnectivity.getConnection();
		Statement stmt = getStatement(conn);
		ResultSet rs = null;
		try {
			// create student user
			PreparedStatement deleteUser = conn.prepareStatement("DELETE FROM course WHERE alias=?;");
			deleteUser.setString(1, course.getAlias());
			deleteUser.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			close(conn, stmt);
		}
		return course;
	}

	// Enrollments && Courses Taught
	// *******
	// Create Enrollment
	// create enrollment object in the database
	public static void writeEnrollmentToDB(Enrollment enrollment){
		Connection conn = DatabaseConnectivity.getConnection();
		Statement stmt = getStatement(conn);
		ResultSet rs = null;
		try {
			// create student user
			PreparedStatement createEnrollment = conn.prepareStatement("INSERT INTO enrollment (studentEmail, courseAlias, grade, alias) VALUES (?, ?, ?, ?)");
			createEnrollment.setString(1, enrollment.getStudent().getEmail());
			createEnrollment.setString(2, enrollment.getCourse().getAlias());
			createEnrollment.setString(3, enrollment.getGrade().name());
			createEnrollment.setString(4, enrollment.getAlias());
			createEnrollment.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			close(conn, stmt);
		}
	}

	public static ArrayList<Course> getCoursesTaughtByFacultyEmail(String email){
		Connection conn = getConnection();
		Statement stmt = getStatement(conn);
		ResultSet rs = null;
		ArrayList<Course> courses = new ArrayList<Course>();
		try {
			rs = stmt.executeQuery("SELECT * FROM course where facultyEmail='" + email + "';");
			while (rs.next()) {
				String department = rs.getString("department");
				int courseNum = rs.getInt("courseNum");
				String quarterAlias = rs.getString("quarter");
				Quarter quarter = getQuarterByAlias(quarterAlias);
				courses.add(new Course(Department.valueOf(department.toUpperCase()), courseNum, quarter));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			close(conn, stmt);
		}
		return courses;
	}

	// Quarter Management
	// *************
	public static Quarter getQuarterByAlias(String alias){
		Connection conn = getConnection();
		Statement stmt = getStatement(conn);
		ResultSet rs = null;
		Quarter quarter = null;
		try {
			rs = stmt.executeQuery("SELECT * FROM quarter WHERE alias='" + alias +"'");
			Date startDate = null;
			Date endDate = null;
			Date startRegDate = null;
			Date endRegDate = null;
			int count = 0;
			while (rs.next()) {
				startDate = rs.getDate("startDate");
				endDate = rs.getDate("endDate");
				startRegDate = rs.getDate("startRegDate");
				endRegDate = rs.getDate("endRegDate");
				count++;
			}
			if(count == 0) return null;
			quarter = new Quarter(alias, startDate, endDate, startRegDate, endRegDate);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			close(conn, stmt);
		}
		return quarter;
	}

	// Room Management
	// *************
	public static ArrayList<Room> getAllRooms(){
		Connection conn = getConnection();
		Statement stmt = getStatement(conn);
		ResultSet rs = null;
		ArrayList<Room> rooms = new ArrayList<Room>();
		try {
			rs = stmt.executeQuery("SELECT * FROM room");
			while (rs.next()) {
				String name = rs.getString("name");
				int capacity = rs.getInt("capacity");
				rooms.add(new Room(name, capacity));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			close(conn, stmt);
		}
		return rooms;
	}

	public static void writeRoomToDB(Room room){
		Connection conn = DatabaseConnectivity.getConnection();
		Statement stmt = getStatement(conn);
		ResultSet rs = null;
		try {
			// create student user
			PreparedStatement createUser = conn.prepareStatement("INSERT INTO room (name, capacity) VALUES (?, ?)");
			createUser.setString(1, room.getName());
			createUser.setInt(2, room.getCapacity());
			createUser.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			close(conn, stmt);
		}
	}

	public static Room getRoomByName(String name){
		Connection conn = getConnection();
		Statement stmt = getStatement(conn);
		ResultSet rs = null;
		Room room = null;
		try {
			rs = stmt.executeQuery("SELECT * FROM room WHERE name='" + name +"'");
			int capacity = -1;
			int id = -1;
			int count = 0;
			while (rs.next()) {
				capacity = rs.getInt("capacity");
				// id = rs.getInt("id");
				count++;
			}
			if(count == 0) return null;
			room = new Room(name, capacity);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			close(conn, stmt);
		}
		return room;
	}

	public static void updateRoom(Room room){
		if (room == null) return;
		Connection conn = DatabaseConnectivity.getConnection();
		Statement stmt = getStatement(conn);
		ResultSet rs = null;
		try {
			PreparedStatement updateCapacity = conn.prepareStatement("UPDATE room SET capacity=? WHERE name=?;");
			updateCapacity.setInt(1, room.getCapacity());
			updateCapacity.setString(2, room.getName());
			updateCapacity.executeUpdate();
		} catch(SQLException e) {
			e.printStackTrace();
		}finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			close(conn, stmt);
		}
	}

	public static void deleteRoomFromDB(Room room){
	}

	// DELETE RESOURCE METHOD
	// Use same one since same structure
	// **********************
	public static void deleteResourceGivenID(String query, int id) {
		Connection conn = DatabaseConnectivity.getConnection();
		Statement stmt = getStatement(conn);
		ResultSet rs = null;
		try {
			// create student user
			PreparedStatement deleteUser = conn.prepareStatement(query);
			deleteUser.setInt(1, id);
			deleteUser.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			close(conn, stmt);
		}
	}

	// **********************
	// FORCE QUERIES (TESTING)
	// *********************
	public static void forceQuery(String query) {
		Connection conn = DatabaseConnectivity.getConnection();
		Statement stmt = getStatement(conn);
		ResultSet rs = null;
		try {
			// create student user
			PreparedStatement deleteUser = conn.prepareStatement(query);
			deleteUser.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			close(conn, stmt);
		}
	}

	// ***************
	// Quarter Management
	// ***************
	public static Quarter getQuarterGivenAlias(String alias) {
		Quarter quarter = null;
		Connection conn = getConnection();
		Statement stmt = getStatement(conn);
		ResultSet rs = null;
		try {
			rs = stmt.executeQuery("SELECT * FROM quarter WHERE alias=" + alias);
			Date startDate = null;
			Date endDate = null;
			Date startregDate = null;
			Date endregDate = null;
			int count = 0; // use to check if result is null
			while (rs.next()) {
				startDate = rs.getDate("startDate");
				endDate = rs.getDate("endDate");
				startregDate = rs.getDate("startregDate");
				endregDate = rs.getDate("endregDate");
				count++;
			}
			if (count == 0) return quarter;
			quarter = new Quarter(alias, startDate, endDate, startregDate, endregDate);
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			close(conn, stmt);
		}
		return quarter;
	}

	public static ArrayList<Quarter> getCurrentFutureQuarters(){
		Connection conn = getConnection();
		Statement stmt = getStatement(conn);
		ResultSet rs = null;
		ArrayList<Quarter> quarters = new ArrayList<Quarter>();
		try {
			rs = stmt.executeQuery("SELECT * FROM quarter;");
			while (rs.next()) {
				String alias = rs.getString("alias");
				Date startDate = rs.getDate("startDate");
				Date endDate = rs.getDate("endDate");
				Date startregDate = rs.getDate("startregDate");
				Date endregDate = rs.getDate("endregDate");
				if (endDate.after(new Date()))
					quarters.add(new Quarter(alias, startDate, endDate, startregDate, endregDate));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			close(conn, stmt);
		}
		return quarters;
	}

}