package com.mpcs51410.uniapp.model;
import javax.xml.crypto.Data;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Enrollment {
	private Course course;
	private Student student;
	private Grade grade;
	private String alias;
	
	public Enrollment(Course course, Student student, Grade grade) {
		if (course == null || student == null || grade == null)
			throw new RuntimeException("Provide valid inputs to create an enrollment");
		this.course = course;
		this.student = student;
		this.grade = grade;
		String[] emailParts= student.getEmail().split("@");
		this.alias =  emailParts[0] + "-" + course.getAlias();
	}
	// setter methods
	public void setGrade(Grade grade) {
		this.grade = grade;
		DatabaseConnectivity.forceQuery("UPDATE enrollment SET grade='" + grade .toString()+ "' WHERE alias='" + this.alias + "';");
	}
	
	// getter methods
	public Course getCourse() {
		return this.course;
	}
	
	public Student getStudent() {
		return this.student;
	}
	
	public Grade getGrade() {
		return this.grade;
	}
	
	public String getAlias() {
		return this.alias;
	}
	
	// equals method
	public boolean equals(Enrollment e) {
		if(e.getAlias().toLowerCase().equals(this.alias.toLowerCase()))
			return true;
		else return false;
	}

	public void writeToDB(){
		DatabaseConnectivity.forceQuery("INSERT INTO enrollment (studentEmail, courseAlias, grade, alias) " +
				"VALUES ('" + this.student.getEmail()+"', '" + this.course.getAlias() + "', '" + this.grade +"', '" + this.alias + "')");
	}
}
