package com.mpcs51410.uniapp.model;
import java.util.Date;

public class RegistrarForm {
	private Student student;
	private Course course;
	private Action action;
	private Date date;
	private boolean isActionSuccessful;
	
	// Registration constructor
	public RegistrarForm(Student student, Course course, Action action) {
		if(student == null || course == null)
			throw new RuntimeException("Provide a valid student and course for a registration");
		if (!(action == Action.ADD || action == Action.DROP))
			throw new RuntimeException("Provide a valid action (either 'add' or 'drop')");
		
		// attempt to add student if action = "add"
		if (action == Action.ADD)
			this.isActionSuccessful = registerStudentToCourse(student, course);
		
		// attempt to drop student if action = "drop"
		if (action == Action.DROP) 
			this.isActionSuccessful = dropStudentFromCourse(student, course);
		
		this.student = student;
		this.course = course;
		this.action = action;
		this.date = new Date(); // set date to date/time when action was done
		
	}
		
	// register a student to a course
	// this is where the checks happen (e.g. if course if full
	// or if student has restriction) needed for successful registration
	// return true if registration was successful; false otherwise
	public static boolean registerStudentToCourse(Student student, Course course){
		if(course.getIsOpen() == false) return false;
		if(course.isClassFull()) return false;
		if(student.hasReachedEnrollmentLimit()) return false;
		if(!(course.getQuarter().isRegistrationPeriodOn())) return false;
		// add student to class roster
		return course.addStudent(student);
	}
	
	// drop a student from course
	// returns true if student is drop action is successful
	public static boolean dropStudentFromCourse(Student student, Course course) {
		if(!course.getQuarter().isRegistrationPeriodOn()){
			System.out.println("Course registration period is not on");
			return false;
		} else {
			boolean isRemoveSuccessful = course.removeStudent(student);
			System.out.println("removing student from course roster was successful: " +  isRemoveSuccessful);
			return isRemoveSuccessful;
		}
	}
	
	// Comparison method
	public boolean equals(RegistrarForm form) {
		Student compStudent = form.getStudent();
		Course compCourse = form.getCourse();
		if (this.student.equals(compStudent) && this.course.equals(compCourse)) return true;
		else return false;
	}
	
	// getter methods
	public Student getStudent() {
		return this.student;
	}
	
	public Course getCourse() {
		return this.course;
	}
	
	public Date getDate() {
		return this.date;
	}
	
	public Action action() {
		return this.action;
	}
	
	public boolean getIsActionSuccessful() {
		return this.isActionSuccessful;
	}
}
