package com.mpcs51410.uniapp.model;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Room implements ReadableWritable {

	private String name;
	private int capacity;
	private int id;
	
	public Room(String name, int capacity) {
		if(name == null)
			throw new RuntimeException("Provide valid name for room");
		this.name = name;
		this.capacity = capacity;
	}
	// second constructor with id
	public Room(String name, int capacity, int id) {
		if(name == null)
			throw new RuntimeException("Provide valid name for room");
		this.name = name;
		this.capacity = capacity;
		this.id = id;
	}
	
	// setter methods
	public void setCapacity(int capacity) {
		this.capacity = capacity;
		DatabaseConnectivity.updateRoom(this);
	}
	
	// getter methods
	public String getName() { return this.name; }

	public int getCapacity() {
		return this.capacity;
	}
	

	// Database Methods
	public void writeToDB(){
		DatabaseConnectivity.forceQuery("INSERT INTO room (name, capacity) VALUES ('" + this.name + "', " + this.capacity + ");");
	}

	public ReadableWritable readFromDB(int id){
		Room room = DatabaseConnectivity.getRoomByName(this.name);
		return room;
	}

	public void deleteFromDB(){
		DatabaseConnectivity.forceQuery("DELETE FROM room WHERE name='" + this.name +"';");
	}

	// equals
	public boolean equals(Room room){
		if (this.name.toUpperCase().equals(room.getName().toUpperCase()))
			return true;
		else return false;
	}
}
