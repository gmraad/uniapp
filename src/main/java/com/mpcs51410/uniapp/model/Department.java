package com.mpcs51410.uniapp.model;

public enum Department {
	MPCS, HIST, GEOG, MATH
}
