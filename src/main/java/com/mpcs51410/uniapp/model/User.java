package com.mpcs51410.uniapp.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public abstract class User implements ReadableWritable {
	private String name;
	private String email;
	private int id;
	private UserType usertype;
	
	public User(String name, String email, int id) {
		if (name == null)
			throw new RuntimeException("A user must have a name");
		if (!email.matches("^([_a-zA-Z0-9-])+(\\.[_a-zA-Z0-9-]+)*@uchicago.edu"))
			throw new RuntimeException("Provided e-mail is not valid");
		this.name = name;
		this.email = email;
		this.id = id;
		// Student id must be between 100,000 and 999,999
		if(id > 99999 && id < 1000000) {
			this.usertype = UserType.STUDENT;
		}
		// Faculty id must be between 10,000 and 99,9999
		if (id > 9999 && id < 100000) {
			this.usertype = UserType.FACULTY;
		}
		// Administrator id must be betwween 100 and 999
		if (id > 99 && id < 1000) {
			this.usertype = UserType.ADMINISTRATOR;
		}
		// if after checking id ranges, do not find
		// match, throw exception
		if(id == -1) throw new RuntimeException("Provide valid id");
	}
	
	// check whether user exists in database
	public boolean existsInDatabase() {
		ReadableWritable user = this.readFromDB(this.id);
		if (user == null) return false;
		else return true;
	}

	// getter methods
	public String getName() {
		return this.name;
	}
	
	public String getEmail() {
		return this.email;
	}
	
	public int getID() {
		return this.id;
	}

	public UserType getUsertype() { return this.usertype; }

	// ***** Database Methods *****
	// (ReadableWritable Interface)

	// write
	public void writeToDB() {
		DatabaseConnectivity.writeUserToDB(this);
		if (this.usertype == UserType.STUDENT){
			DatabaseConnectivity.forceQuery("INSERT INTO student (email, hasRestrictions) VALUES ('" + this.email
					+ "', "+ false + ");");
		}
	}

	// read
	public ReadableWritable readFromDB(int id) {
		User user = DatabaseConnectivity.getUserGivenID(id);
		return user;
	}

	// delete
	public void deleteFromDB() {
		UserType type = this.usertype;
		DatabaseConnectivity.deleteResourceGivenID("DELETE FROM user WHERE id = ?;", this.id);
		if (type == UserType.STUDENT) {
			DatabaseConnectivity.forceQuery("DELETE FROM student WHERE email='" + this.email + "';");
			DatabaseConnectivity.forceQuery("DELETE FROM enrollment WHERE studentEmail='" + this.email + "';");
		}
		if (type == UserType.FACULTY) {
			DatabaseConnectivity.forceQuery("UPDATE course SET facultyEmail=NULL where facultyEmail='" + this.email + "';");
		}
	}

}
