package com.mpcs51410.uniapp.model;
import java.time.DayOfWeek;
import java.sql.Time;

public class TimeBlock {
	private DayOfWeek day; // 1 is Monday; 7 is Sunday
	private Time startTime;
	private Time endTime;
	
	public TimeBlock(DayOfWeek day, Time startTime, Time endTime) {
		if (day == DayOfWeek.SUNDAY)
			throw new RuntimeException("Classes cannot be scheduled on Sunday");
		if(startTime.after(endTime))
			throw new RuntimeException("Start time cannot be after end time");
		this.day = day;
		this.startTime = startTime;
		this.endTime = endTime;
	}
	
	// getter methods
	public DayOfWeek getDay() {
		return this.day;
	}
	
	public Time getStartTime() {
		return this.startTime;
	}
	
	public Time getEndTime() {
		return this.endTime;
	}
	
	// equals method
	// check that two time blocks are the same
	public boolean equals(TimeBlock tb) {
		if(this.day == tb.getDay() &&
				this.startTime.equals(tb.getStartTime())&&
				this.endTime.equals(tb.getEndTime()))
			return true;
		else return false;
	}
}
