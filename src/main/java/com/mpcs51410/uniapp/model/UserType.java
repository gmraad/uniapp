package com.mpcs51410.uniapp.model;

public enum UserType {
	FACULTY, STUDENT, ADMINISTRATOR
}
