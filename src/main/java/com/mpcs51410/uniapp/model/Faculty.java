package com.mpcs51410.uniapp.model;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

public class Faculty extends User {
	private int id;
	private ArrayList<Course> courses;
	
	public Faculty(String name, String email, int id) {
		// construct User
		super(name, email, id);
		this.courses = new ArrayList<Course>();
	}

	public void loadCoursesTaught(){
		this.courses = DatabaseConnectivity.getCoursesTaughtByFacultyEmail(this.getEmail());
	}
	public ArrayList<Course> getCourses() {
		if (this.courses.size() == 0 || this.courses == null) this.loadCoursesTaught();
		return this.courses;
	}


	// assign given grade of given student in given course
	public void assignStudentGradeInCourse(Student student, Course course, Grade grade) {
		student.studentEnrollmentInCourse(course).setGrade(grade);
	}
}
