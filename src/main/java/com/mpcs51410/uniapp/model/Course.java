package com.mpcs51410.uniapp.model;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

public class Course implements ReadableWritable {
	// Mandatory in constructor
	private int id;
	private Department department;
	private int courseNumber;
	private Quarter quarter;
	// do not pass in as arguments to constructor
	private ArrayList<Student> roster; // new ArrayList auto created
	private Faculty faculty; // null in constructor
	private Room room; // null in constructor
	private ArrayList<TimeBlock> meetingTimes; // new ArrayList in constructor
	private boolean isOpen; // true in constructor
	private String alias;

	// minimal constructor for a course
	public Course(Department department, int courseNum){
		if(department == null)
			throw new RuntimeException("Provide at least a valid deparment to create course.");
		// check if course number is valid
		if(!(courseNum > 9999 && courseNum < 100000))
			throw new RuntimeException("course number must be between 10,000 and 99,999");
		// passed checks; create course
		this.department = department;
		this.courseNumber = courseNum;
		this.quarter = null;
		this.roster = new ArrayList<Student>();
		this.faculty = null;
		this.room = null;
		this.meetingTimes = new ArrayList<TimeBlock>();
		this.isOpen = true;
		this.alias = department.name() +"-" + courseNum;
		this.id = id;
	}

	// to create a course, a department a course number, roster (even if empty) MUST be provided
	// a Faculty and a Room are optional (i.e. can be set to null in the constructor) and can be set later
	public Course(Department department, int courseNum, Quarter quarter) {
		// check if department code is valid
		if(department == null)
			throw new RuntimeException("Provide at least a valid deparment to create course.");		
		// check if course number is valid
		if(!(courseNum > 9999 && courseNum < 100000))
			throw new RuntimeException("course number must be between 10,000 and 99,999");
		// check if quarter is valid
		if (quarter == null)
			throw new RuntimeException("quarter must be valid");
		
		// passed checks; create course
		this.department = department;
		this.courseNumber = courseNum;
		this.quarter = quarter;
		this.roster = new ArrayList<Student>();
		this.faculty = null;
		this.room = null;
		this.meetingTimes = new ArrayList<TimeBlock>();
		this.isOpen = true;
		this.alias = department.name() +"-" + courseNum + "-" + quarter.getAlias();
	}
	
	// add student to course
	// returns student if registration was successful
	// return null was unsuccessful
	public boolean addStudent(Student student) {
		return this.roster.add(student);
	}
	// drop student from course
	// returns true if student was enrolled in course and remove successful
	// Returns false if student is not enrolled; roster remains unchanged
	public boolean removeStudent(Student student) {
		// lookup student in roster with given student's e-mail
		Student toRemove = null;
		for (Student s : roster){
			if (s.getEmail().equals(student.getEmail()))
				toRemove = s;
		}
		if (toRemove == null) return false;
		else return this.roster.remove(toRemove);
	}
	
	// Check if class is full
	// return true if it is; false if there are seats left
	public boolean isClassFull() {
		if (this.room == null) return true;
		int remainingSeats = room.getCapacity() - roster.size();
		if(remainingSeats > 0) return false;
		else return true;
	}
	
	// Check if two courses are the same
	// equality holds only if the content is the same
	// i.e. they share department and course number
	public boolean equals(Course course) {
		Department compDepartment = course.getDepartment();
		int compNum = course.getCourseNumber();
		if(this.department == compDepartment && this.courseNumber == compNum) return true;
		else return false;
	}
	
	// setter methods
	// ********************************
		
	public void setFaculty(Faculty faculty) {
		this.faculty = faculty;
	}
	public void setRoom(Room room) {
		// check that roster's size is less than the room's capacity
		if (room != null && this.roster.size() > room.getCapacity())
			throw new RuntimeException("Room is not big enough for class");
		this.room = room;
		DatabaseConnectivity.forceQuery("UPDATE course SET roomName='" + this.room.getName() + "' WHERE alias='" + this.alias +"';");
	}

	public void setQuarter(Quarter quarter) {this.quarter = quarter;}
	
	public void setMeetingTimes(ArrayList<TimeBlock> meetingTimes) {
		this.meetingTimes = meetingTimes;
	}
	
	public void setIsOpen(boolean isOpen) {
		this.isOpen = isOpen;
		/*
		// update course in the database
		Connection conn = DatabaseConnectivity.getConnection();
		try {
			PreparedStatement updateIsOpen = conn.prepareStatement("UPDATE course SET isOpen =? WHERE alias='" + this.alias + "'");
			updateIsOpen.setBoolean(1, isOpen);
			updateIsOpen.executeUpdate();
		} catch(SQLException e) {
			e.printStackTrace();
		}*/
	}

	public void updateAlias(){
		this.alias = department.name() +"-" + this.courseNumber + "-" + quarter.getAlias();
	}

	// getter methods
	// ********************************

	public int getID() { return this.id; }

	public Department getDepartment() {
		return this.department;
	}
	
	public int getCourseNumber() {
		return this.courseNumber;
	}
	
	public Quarter getQuarter() {
		return this.quarter;
	}
	
	public ArrayList<Student> getRoster() {
		if (this.roster.size() == 0 || this.roster == null)
			this.loadRoster();
		return this.roster;
	}
	
	public Faculty getFaculty() {
		return this.faculty;
	}
	
	public Room getRoom() {
		return this.room;
	}
	
	public ArrayList<TimeBlock> getMeetingTimes(){
		return this.meetingTimes;
	}
	
	public boolean getIsOpen() {
		return this.isOpen;
	}
	
	public String getAlias() {
		return this.alias;
	}
	
	// toString method
	public String toString() {
		return department + ":" + courseNumber;
	}

	// Database Methods
	// **************************

	public void loadRoster(){
		this.roster = DatabaseConnectivity.getCourseRosterByAlias(this.getAlias());
	}

	public void writeToDB() {
		DatabaseConnectivity.writeCourseToDB(this);
	}

	public ReadableWritable readFromDB(int id){
		return DatabaseConnectivity.getCourseGivenID(id);
	}

	public void deleteFromDB(){
		DatabaseConnectivity.forceQuery("DELETE FROM enrollment WHERE courseAlias='" + this.alias +"';");
		DatabaseConnectivity.deleteCourse(this);
	}
}
